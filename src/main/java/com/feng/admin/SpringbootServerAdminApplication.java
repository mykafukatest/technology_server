package com.feng.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

import java.util.HashMap;

@SpringBootApplication
@MapperScan("com.feng.admin.mapper")
public class SpringbootServerAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootServerAdminApplication.class, args);
    }

}
