package com.feng.admin.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Component
@ConfigurationProperties(prefix="jwt.token")
public class JwtConfigProperties {
    //密钥
    private String secret;
    //token过期时间
    private int accexpiration;
    //刷新token过期时间
    private int refexpiration;
    //登录url
    private String loginUrl;
    //退出url
    private String logoutUrl;
}