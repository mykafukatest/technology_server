package com.feng.admin.config;

import cn.hutool.json.JSONUtil;
import com.feng.admin.result.ResponseResult;
import com.feng.admin.security.*;
import com.feng.admin.service.JwtTokenService;
import com.nimbusds.jose.JOSEException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;

/**
 * @author lbl
 * @date 2019/10/16 20:25
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Slf4j
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    @Qualifier("userDetailsServiceImpl")
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    private IgnoreUrlsConfigProperties ignoreUrlsConfigProperties;

    @Autowired
    private JwtConfigProperties jwtConfigProperties;

    @Autowired
    private JwtTokenService tokenService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions().disable();
        //使用jwt的Authentication,来解析过来的请求是否有token
        String[] ignoringUrls = ignoreUrlsConfigProperties.getUrls().toArray(new String[0]);
        http.authorizeRequests()
                //这里表示"/any"和"/ignore"不需要权限校验
                .antMatchers(ignoringUrls).permitAll()
                .anyRequest().authenticated()
                .and()
                .cors()
                // 这里表示任何请求都需要校验认证(上面配置的放行)
                .and()
                //配置登录,检测到用户未登录时跳转的url地址,登录放行
                .formLogin()
                //需要跟前端表单的action地址一致
                .loginProcessingUrl(jwtConfigProperties.getLoginUrl())
                .successHandler(loginSuccessHandler())
                .failureHandler(loginFailureHandler())
                .permitAll()
                //配置取消session管理,又Jwt来获取用户状态,否则即使token无效,也会有session信息,依旧判断用户为登录状态
                .and()
                .logout()
                .logoutUrl(jwtConfigProperties.getLogoutUrl())
                .logoutSuccessHandler(logoutHandler())
                .and()
                .exceptionHandling()
                .accessDeniedHandler(customizeAccessDeniedHandler())
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                //配置登出,登出放行
                .and()
                .logout()
                .permitAll()
                .and()
                .csrf().disable();

        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(customAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
    @Bean
    public CustomAuthenticationFilter customAuthenticationFilter() throws Exception {
        CustomAuthenticationFilter filter=new CustomAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManager());
        filter.setFilterProcessesUrl(jwtConfigProperties.getLoginUrl());
        filter.setAuthenticationSuccessHandler(loginSuccessHandler());
        filter.setAuthenticationFailureHandler(loginFailureHandler());
        return filter;
    }

    @Bean
    public LoginSuccessHandler loginSuccessHandler(){
        return new LoginSuccessHandler();
    }
    @Bean
    public LogoutHandler logoutHandler(){
        return new LogoutHandler();
    }
    @Bean
    public LoginFailureHandler loginFailureHandler(){
        return new LoginFailureHandler();
    }

    @Bean
    public CustomizeAccessDeniedHandler customizeAccessDeniedHandler(){
        return new CustomizeAccessDeniedHandler();
    }

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                // 设置UserDetailsService
                .userDetailsService(userDetailsService)
                //
                .passwordEncoder(passwordEncoder());
    }
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
