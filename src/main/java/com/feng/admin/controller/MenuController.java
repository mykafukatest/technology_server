package com.feng.admin.controller;

import com.feng.admin.service.PermissionService;
import com.feng.admin.vo.PermissionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lbl
 * @date 2020/5/15 11:00
 */
@RestController
@Api(tags = "菜单模块")
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private PermissionService permissionService;

    @GetMapping("/getMenuInfo")
    @ApiOperation("查询菜单")
    public List<PermissionVo> queryTheCurrentUserSMenu(){
        return permissionService.queryTheCurrentUserSMenu();
    }

}
