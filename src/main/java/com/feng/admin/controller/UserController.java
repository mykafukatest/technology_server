package com.feng.admin.controller;


import com.feng.admin.annotation.SystemLog;
import com.feng.admin.entity.Role;
import com.feng.admin.entity.User;
import com.feng.admin.result.PageResult;
import com.feng.admin.result.ResponseResult;
import com.feng.admin.service.RoleService;
import com.feng.admin.service.UserService;
import com.feng.admin.vo.UserVo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
@Api(tags = "用户模块")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @GetMapping("/getInfo")
    public ResponseResult getUserInfo(){
        UserVo userVo = userService.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        return ResponseResult.success(userVo);
    }

    @GetMapping("/findAll")
    public ResponseResult findAll(){
        return ResponseResult.success(userService.findAll());
    }

    @GetMapping("/findPage")
    public ResponseResult findPage(int page, int size){
        PageInfo<User> info = userService.findPage(page, size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @PostMapping("/search")
    public ResponseResult findList(@RequestBody Map<String,Object> searchMap){
        return ResponseResult.success(userService.findList(searchMap));
    }


    @PostMapping("/search/{page}/{size}")
    @PreAuthorize("hasAuthority('USER_QUERY')")
    public ResponseResult findPage(@RequestBody Map<String,Object> searchMap,
                                             @PathVariable("page") int page,
                                             @PathVariable("size") int size){
        PageInfo<User> info =userService.findPage(searchMap,page,size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @GetMapping("/findById/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        return ResponseResult.success(userService.findById(id));
    }


    @PostMapping("/save")
    @PreAuthorize("hasAuthority('USER_ADD')")
    @SystemLog(module = "用户模块添加用户",method = "add")
    public ResponseResult add(@RequestBody User user){
        userService.add(user);
        return ResponseResult.success();
    }
    @PreAuthorize("hasAuthority('USER_UPDATE')")
    @SystemLog(module = "用户模块更新用户",method = "update")
    @PostMapping("/update")
    public ResponseResult update(@RequestBody User user){
        userService.update(user);
        return ResponseResult.success();
    }

    @GetMapping("/delete/{id}")
    @SystemLog(module = "用户模块删除用户",method = "delete")
    @PreAuthorize("hasAuthority('USER_DELETE')")
    public ResponseResult delete(@PathVariable("id")Long id){
        userService.delete(id);
        return ResponseResult.success();
    }

    @GetMapping("/findRolesByUserId/{userId}")
    public ResponseResult findRolesByUserId(@PathVariable("userId")Long userId){
        List<Role> userRoles = userService.findRolesByUserId(userId);
        List<Role> allRoles = roleService.findAll();
        Map<String,Object> result=new HashMap<>();
        result.put("userRoles",userRoles.stream().map(Role::getId).collect(Collectors.toList()));
        result.put("allRoles",allRoles);
        return ResponseResult.success(result);
    }

    @PostMapping("/addRoleByUserId/{userId}")
    @SystemLog(module = "用户模块分配角色",method = "addRoleByUserId")
    @PreAuthorize("hasAuthority('USER_ADDROLEBYUSERID')")
    public ResponseResult addRoleByUserId(@PathVariable("userId")Long userId, @RequestBody List<Long> roleIds){
        //分配权限
        userService.assigningRolesByUserId(userId, roleIds);

        return ResponseResult.success();
    }
}
