package com.feng.admin.controller;

import com.feng.admin.result.ResponseResult;
import com.feng.admin.utils.VerifyCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/verify")
public class VerifyController {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    @Value("${verification.redis.prefix}")
    private String VERIFICATION_KEYPREFIX;

    /**
     * 生成验证码
     * @param key
     * @return
     * @throws IOException
     */
    @GetMapping("/verification")
    public ResponseResult<String> verification(@RequestParam("key")String key) throws IOException {
        //生成随机字串
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        redisTemplate.opsForValue().set(VERIFICATION_KEYPREFIX+key,verifyCode,3, TimeUnit.MINUTES);
        //输出到图片
        ByteArrayOutputStream data = new ByteArrayOutputStream();
        VerifyCodeUtils.outputImage(100,40,data,verifyCode);
        //把图片转base64返回
        BASE64Encoder encoder = new BASE64Encoder();
        return ResponseResult.success("data:image/png;base64,"+encoder.encode(data.toByteArray()));
    }

}
