package com.feng.admin.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * log实体类
 * @author Administrator
 *
 */
@Data
@Table(name="tb_log")
public class Log implements Serializable{
	/**
 	 * id
	*/
	@Id
	private Integer id;
	/**
 	 * username
	*/
	private String username;
	/**
 	 * ip
	*/
	private String ip;
	/**
 	 * nickname
	*/
	private String nickname;
	/**
 	 * url
	*/
	private String url;
	/**
 	 * module
	*/
	private String module;
	/**
 	 * method
	*/
	private String method;
	/**
 	 * action
	*/
	private String action;
	/**
 	 * description
	*/
	private String description;
	/**
	 * create
	 */
	@Column(name = "`create`")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date create;
	/**
 	 * status
	*/
	@Column(name = "`status`")
	private String status;


	@Column(name = "params")
	private String params;

}
