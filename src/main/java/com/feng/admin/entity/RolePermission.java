package com.feng.admin.entity;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * rolePermission实体类
 * @author Administrator
 *
 */
@Data
@Table(name="tb_role_permission")
public class RolePermission implements Serializable{
	/**
 	 * id
	*/
	@Id
	private Long id;
	/**
 	 * 角色 ID
	*/
	private Long roleId;
	/**
 	 * 权限 ID
	*/
	private Long permissionId;
}
