package com.feng.admin.entity;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * userRole实体类
 * @author Administrator
 *
 */
@Data
@Table(name="tb_user_role")
public class UserRole implements Serializable{
	/**
 	 * id
	*/
	@Id
	private Long id;
	/**
 	 * 用户 ID
	*/
	private Long userId;
	/**
 	 * 角色 ID
	*/
	private Long roleId;
}
