package com.feng.admin.exception;

import com.nimbusds.jose.JOSEException;

public class JwtInvalidException extends JOSEException {

    public JwtInvalidException(String message) {
        super(message);
    }
}
