package com.feng.admin.mapper;


import com.feng.admin.entity.Permission;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PermissionMapper extends Mapper<Permission> {
    int updateBatch(List<Permission> list);

    int batchInsert(@Param("list") List<Permission> list);

    int insertOrUpdate(Permission record);

    int insertOrUpdateSelective(Permission record);

    /**
     * 查询用户所拥有的权限或者菜单  type=2是
     * @param username username type 如果为2代表查询菜单
     * @return Permission
     */
    List<Permission> findPermissionByUserName(@Param("username") String username, @Param("type")Integer type);

    /**
     * 查询角色id查询所拥有的权限或者菜单
     * @param roleId roleId角色id
     * @return Permission
     */
    List<Long> queryPermissionsBasedOnRoleId(@Param("roleId") Long roleId);
}