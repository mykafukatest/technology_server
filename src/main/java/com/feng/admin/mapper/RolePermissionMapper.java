package com.feng.admin.mapper;


import com.feng.admin.entity.RolePermission;
import tk.mybatis.mapper.common.Mapper;

public interface RolePermissionMapper extends Mapper<RolePermission> {

}
