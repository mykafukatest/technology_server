package com.feng.admin.mapper;


import com.feng.admin.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {

}
