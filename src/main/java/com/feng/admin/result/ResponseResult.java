package com.feng.admin.result;

import com.feng.admin.constant.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/16/17:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseResult<T> {
    /**
     * 状态
     * @link ResponseCode
     */
    private Integer code;
    /**
     * 响应的消息
     */
    private String msg;
    /**
     * 数据
     */
    private T data;
    /**
     * 操作是否成功
     */
    private boolean success;

    private ResponseResult(Integer code, String msg, boolean success){
        this.code = code;
        this.msg = msg;
        this.success = success;
    }

    /**
     * 带数据的成功
     * @param data 数据
     * @return ResponseResult
     */
    public static <T> ResponseResult<T> success(T data){
        return new ResponseResult<T>(ResponseCode.SUCCESS,"OK",data,true);//
    }

    /**
     * 成功
     * @return ResponseResult
     */
    public static <T> ResponseResult<T> success(){
        return new ResponseResult<T>(ResponseCode.SUCCESS,"OK",true);
    }

    /**
     * 带消息的失败
     * @return ResponseResult
     */
    public static <T> ResponseResult<T> error(String msg){
        return new ResponseResult<T>(ResponseCode.ERROR,msg,null,false);
    }
    /**
     * 失败
     * @return ResponseResult
     */
    public static <T> ResponseResult<T> error(){
        return new ResponseResult<T>(ResponseCode.ERROR,"NO",false);
    }

    /**
     * 未授权
     * @return ResponseResult
     */
    public static <T> ResponseResult<T> unauthorized(){
        return new ResponseResult<T>(ResponseCode.UNAUTHORIZED,"你没有此操作权限",false);
    }
}
