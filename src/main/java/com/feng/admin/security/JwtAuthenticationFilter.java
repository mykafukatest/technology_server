package com.feng.admin.security;

import cn.hutool.json.JSONUtil;
import com.feng.admin.config.IgnoreUrlsConfigProperties;
import com.feng.admin.config.JwtConfigProperties;
import com.feng.admin.constant.AuthConstant;
import com.feng.admin.dto.PayloadDto;
import com.feng.admin.result.JwtResult;
import com.feng.admin.service.JwtTokenService;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import reactor.core.publisher.Mono;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @date 2019/10/16 20:28
 */
@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private IgnoreUrlsConfigProperties ignoreUrlsConfigProperties;

    @Autowired
    private JwtConfigProperties jwtConfigProperties;

    @Autowired
    private JwtTokenService jwtTokenService;

    //解析 jwt  将用户信息存入SecurityContextHolder中
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        //登录页面放行
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        String uri = request.getRequestURI();
        PathMatcher pathMatcher = new AntPathMatcher();
        //白名单路径直接放行
        List<String> ignoreUrls = ignoreUrlsConfigProperties.getUrls();
        for (String ignoreUrl : ignoreUrls) {
            if (pathMatcher.match(request.getContextPath()+"/"+ignoreUrl, uri)) {
                chain.doFilter(request, response);
                return;
            }
        }
        String access_token = request.getHeader(AuthConstant.JWT_TOKEN_HEADER);
        if(StringUtils.isEmpty(access_token)){
            access_token = request.getParameter("token");
        }
        if (StringUtils.isEmpty(access_token)) {
            response.getWriter().print(JSONUtil.toJsonStr(JwtResult.error("请携带token")));
            response.getWriter().flush();
            response.getWriter().close();
            return;
        }
        try {
            //解析jwt
            PayloadDto dto=jwtTokenService.checkToken(access_token);
            UsernamePasswordAuthenticationToken authenticationToken=new UsernamePasswordAuthenticationToken(dto.getUsername(),null
                    ,dto.getAuthorities().stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList()));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            chain.doFilter(request, response);
        } catch (Exception e) {
            response.getWriter().print(JSONUtil.toJsonStr(JwtResult.error(e.getMessage())));
            response.getWriter().flush();
            response.getWriter().close();
        }
    }

}
