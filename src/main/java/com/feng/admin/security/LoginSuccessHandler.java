package com.feng.admin.security;
import cn.hutool.json.JSONUtil;
import com.feng.admin.entity.Log;
import com.feng.admin.result.JwtResult;
import com.feng.admin.service.JwtTokenService;
import com.feng.admin.service.LogService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * @author lbl
 * @date 2019/10/16 20:28 jwt成功生成报token
 */
@Slf4j
public class LoginSuccessHandler implements AuthenticationSuccessHandler {


    @Autowired
    private LogService logService;

    @Autowired
    private JwtTokenService jwtTokenService;
    /**
     * 登录验证通过生产jwt返回客户端
     * @param request request
     * @param response response
     * @throws IOException
     * @throws ServletException
     */
    @SneakyThrows
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
//        logExecutorService.execute(()->{
//            Log logBean = LogUtils.createLogInfo(request, authentication, "系统模块", "用户登录");
//            logBean.setUrl(loginUrl);
//            logService.add(logBean);
//        });
        response.getWriter().print(JSONUtil.toJsonStr(JwtResult.success(jwtTokenService.grenerateAccessToken(authentication)
                ,jwtTokenService.grenerateRefreshToken(authentication))));
        response.getWriter().flush();
        response.getWriter().close();
    }
}
