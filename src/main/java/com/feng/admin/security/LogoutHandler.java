package com.feng.admin.security;
import cn.hutool.json.JSONUtil;
import com.feng.admin.constant.AuthConstant;
import com.feng.admin.result.JwtResult;
import com.feng.admin.result.ResponseResult;
import com.feng.admin.service.JwtTokenService;
import com.feng.admin.service.LogService;
import com.nimbusds.jose.JOSEException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lbl
 * @date 2019/10/16 20:28 退出
 */
@Slf4j
public class LogoutHandler implements LogoutSuccessHandler {

    @Autowired
    private JwtTokenService jwtTokenService;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String access_token = request.getHeader(AuthConstant.JWT_TOKEN_HEADER);
        if(StringUtils.isEmpty(access_token)){
            access_token = request.getParameter("token");
        }
        try {
            log.info("***用户{}退出***:",jwtTokenService.checkToken(access_token).getUsername());
        } catch (JOSEException e) {
            e.printStackTrace();
            log.error("解析jwt",e);
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().print(JSONUtil.toJsonStr(ResponseResult.success()));
        response.getWriter().flush();
        response.getWriter().close();
    }
}
