package com.feng.admin.security;

import cn.hutool.core.util.StrUtil;
import com.feng.admin.entity.Permission;
import com.feng.admin.entity.User;
import com.feng.admin.mapper.PermissionMapper;
import com.feng.admin.mapper.UserMapper;
import com.feng.admin.vo.UserDetailsVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author lbl
 * @date 2019/10/17 10:03 认证
 */
@Service("userDetailsServiceImpl")
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Value("${activeCode.redis.prefix}")
    private String KEYPREFIX;

    @Value("${verification.redis.prefix}")
    private String VERIFICATION_KEYPREFIX;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        ServletRequestAttributes attributes =(ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //获取输入的code
        String code = request.getParameter("code");
        if(StrUtil.isEmpty(code)){
            code=request.getAttribute("code").toString();
        }
        //获取唯一的key
        String key = request.getParameter("key");
        if(StrUtil.isEmpty(key)){
            key=request.getAttribute("key").toString();
        }
        String redisCode = redisTemplate.opsForValue().get(VERIFICATION_KEYPREFIX+key);
        if(StringUtils.isEmpty(code)){
            throw new BadCredentialsException("请填写验证码!");
        }
        if(StringUtils.isEmpty(redisCode)){
            throw new BadCredentialsException("验证码已过期!");
        }
        if(!Objects.equals(redisCode.toUpperCase(),code.trim().toUpperCase())){
            throw new BadCredentialsException("验证码不正确!");
        }
        Example example=new Example(User.class);
        example.createCriteria().andEqualTo("username",username);
        User user = userMapper.selectOneByExample(example);
        List<GrantedAuthority> grantedAuthorities=new ArrayList<>();
        if(user==null){
            throw new UsernameNotFoundException("用户名不存在!");
        }
        List<Permission> permission = permissionMapper.findPermissionByUserName(user.getUsername(),null);
        if(!CollectionUtils.isEmpty(permission)){
            permission.forEach(e->{
                if(Objects.nonNull(e)){
                    grantedAuthorities.add(new SimpleGrantedAuthority(e.getEnname()));
                }
            });
        }
        UserDetailsVo detailsVo = new UserDetailsVo(user.getUsername(), user.getPassword(), grantedAuthorities);
        detailsVo.setId(user.getId());
        detailsVo.setEmail(user.getEmail());
        detailsVo.setPhone(user.getPhone());
        detailsVo.setName(user.getName());
        return detailsVo;
    }
}
