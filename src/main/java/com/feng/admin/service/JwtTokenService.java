package com.feng.admin.service;

import com.feng.admin.dto.PayloadDto;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.KeyLengthException;
import org.springframework.security.core.Authentication;

public interface JwtTokenService {


    /**
     *
     * @param authentication
     * @return token
     * @throws JOSEException
     */
    String grenerateAccessToken(Authentication authentication) throws JOSEException;

    /**
     *
     * @param authentication
     * @return token
     * @throws JOSEException
     */
    String grenerateRefreshToken(Authentication authentication) throws JOSEException;



    /**
     *
     * @param token
     * @return token
     * @throws JOSEException
     */
    PayloadDto checkToken(String token) throws JOSEException;


    PayloadDto getPayloadDto(Authentication authentication,Integer exptime);
}
