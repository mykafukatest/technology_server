package com.feng.admin.service;

import com.feng.admin.entity.Log;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * log业务逻辑层
 */
public interface LogService {


    public List<Log> findAll();


    public PageInfo<Log> findPage(int page, int size);


    public List<Log> findList(Map<String,Object> searchMap);


    public PageInfo<Log> findPage(Map<String,Object> searchMap,int page, int size);


    public Log findById(Integer id);

    public void add(Log log);


    public void update(Log log);


    public void delete(Integer id);

}
