package com.feng.admin.service;


import com.feng.admin.entity.Permission;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * permission业务逻辑层
 */
public interface PermissionService extends PermissionServiceExt {


    public List<Permission> findAll();


    public PageInfo<Permission> findPage(int page, int size);


    public List<Permission> findList(Map<String, Object> searchMap);


    public PageInfo<Permission> findPage(Map<String, Object> searchMap, int page, int size);


    public Permission findById(Long id);

    public void add(Permission permission);


    public void update(Permission permission);


    public void delete(Long id);

}
