package com.feng.admin.service;


import com.feng.admin.entity.Permission;
import com.feng.admin.vo.PermissionVo;

import java.util.List;

/**
 * @author  lbl
 * @date  2020/5/13 13:22
 */
public interface PermissionServiceExt {


    int updateBatch(List<Permission> list);

    int batchInsert(List<Permission> list);

    int insertOrUpdate(Permission record);

    int insertOrUpdateSelective(Permission record);

    List<PermissionVo> queryTheCurrentUserSMenu();
}
