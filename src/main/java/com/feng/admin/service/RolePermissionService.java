package com.feng.admin.service;

import com.feng.admin.entity.RolePermission;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * rolePermission业务逻辑层
 */
public interface RolePermissionService {


    public List<RolePermission> findAll();


    public PageInfo<RolePermission> findPage(int page, int size);


    public List<RolePermission> findList(Map<String, Object> searchMap);


    public PageInfo<RolePermission> findPage(Map<String, Object> searchMap, int page, int size);


    public RolePermission findById(Long id);

    public void add(RolePermission rolePermission);


    public void update(RolePermission rolePermission);


    public void delete(Long id);

}
