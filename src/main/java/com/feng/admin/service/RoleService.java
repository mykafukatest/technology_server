package com.feng.admin.service;


import com.feng.admin.entity.Role;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * role业务逻辑层
 */
public interface RoleService extends RoleServiceExt {


    public List<Role> findAll();


    public PageInfo<Role> findPage(int page, int size);


    public List<Role> findList(Map<String, Object> searchMap);


    public PageInfo<Role> findPage(Map<String, Object> searchMap, int page, int size);


    public Role findById(Long id);

    public void add(Role role);


    public void update(Role role);


    public void delete(Long id);

}
