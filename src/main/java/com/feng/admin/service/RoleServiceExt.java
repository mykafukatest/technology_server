package com.feng.admin.service;


import com.feng.admin.entity.Role;

import java.util.List;
import java.util.Map;

/**
 * @author  lbl
 * @date  2020/5/13 13:22
 */
public interface RoleServiceExt {


    int updateBatch(List<Role> list);

    int batchInsert(List<Role> list);

    int insertOrUpdate(Role record);

    int insertOrUpdateSelective(Role record);

    /**
     * 根据角色id查询所拥有的权限
     * @param roleId roleId
     * @return Map<String,Object> allMenus菜单视图  selectIds分配的权限
     */
    Map<String,Object> queryMenuBasedOnRoleId(Long roleId);

    /**
     * 保存前端分配的权限id
     * @param roleId
     * @param menuList
     */
    void addAuthorityByRoleId(Long roleId, List<Long> menuList);
}
