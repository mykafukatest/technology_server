package com.feng.admin.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.feng.admin.config.JwtConfigProperties;
import com.feng.admin.dto.PayloadDto;
import com.feng.admin.exception.JwtInvalidException;
import com.feng.admin.service.JwtTokenService;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {
    @Autowired
    private JwtConfigProperties jwtConfigProperties;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public String grenerateAccessToken(Authentication authentication) throws JOSEException {

        PayloadDto payloadDto = this.getPayloadDto(authentication,jwtConfigProperties.getAccexpiration());

        return grenerateToken(authentication,payloadDto,jwtConfigProperties.getSecret());
    }

    @Override
    public String grenerateRefreshToken(Authentication authentication) throws JOSEException {
        return grenerateToken(authentication,
                this.getPayloadDto(authentication,jwtConfigProperties.getRefexpiration()),
                jwtConfigProperties.getSecret());
    }

    @Override
    public PayloadDto checkToken(String token) throws JOSEException {
        //从token中解析JWS对象
        JWSObject jwsObject = null;
        try {
            jwsObject = JWSObject.parse(token);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new JwtInvalidException("token无法解析！");
        }
        //创建HMAC验证器
        JWSVerifier jwsVerifier = new MACVerifier(SecureUtil.md5(jwtConfigProperties.getSecret()));
        if (!jwsObject.verify(jwsVerifier)) {
            throw new JwtInvalidException("token签名不合法！");
        }
        String payload = jwsObject.getPayload().toString();
        PayloadDto payloadDto = JSONUtil.toBean(payload, PayloadDto.class);
        if (payloadDto.getExp() < new Date().getTime()) {
            throw new JwtInvalidException("token已过期！");
        }
        return payloadDto;
    }

    private String grenerateToken(Authentication authentication,PayloadDto payloadDto,String secret) throws JOSEException {

        //创建JWS头，设置签名算法和类型
        JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.HS256).
                type(JOSEObjectType.JWT)
                .build();
        //将负载信息封装到Payload中
        Payload payload = new Payload(JSONUtil.toJsonStr(payloadDto));
        //创建JWS对象
        JWSObject jwsObject = new JWSObject(jwsHeader, payload);

        //创建HMAC签名器
        JWSSigner jwsSigner = new MACSigner(SecureUtil.md5(jwtConfigProperties.getSecret()));
        //签名
        jwsObject.sign(jwsSigner);

        return jwsObject.serialize();
    }

    public PayloadDto getPayloadDto(Authentication authentication,Integer exptime) {
        Date now = new Date();
        Date exp = DateUtil.offsetSecond(now,exptime);
        return PayloadDto.builder()
                .sub(authentication.getName())
                .iat(now.getTime())
                .exp(exp.getTime())
                .jti(UUID.randomUUID().toString())
                .username(authentication.getName())
                .authorities(authentication.getAuthorities()
                        .stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList()))
                .build();
    }

}
