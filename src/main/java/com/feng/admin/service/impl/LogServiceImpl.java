package com.feng.admin.service.impl;

import com.feng.admin.entity.Log;
import com.feng.admin.mapper.LogMapper;
import com.feng.admin.service.LogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Log> findAll() {
        return logMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageInfo<Log> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<Log> pageInfo=new PageInfo<>(logMapper.selectAll());
        return pageInfo;
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Log> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return logMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageInfo<Log> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size,"`create` desc");
        Example example = createExample(searchMap);
        PageInfo<Log> pageInfo=new PageInfo<>(logMapper.selectByExample(example));
        return pageInfo;
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Log findById(Integer id) {
        return logMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param log
     */
    public void add(Log log) {
        logMapper.insert(log);
    }

    /**
     * 修改
     * @param log
     */
    public void update(Log log) {
        logMapper.updateByPrimaryKeySelective(log);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Integer id) {
        logMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Log.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // username
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }
            // ip
            if(searchMap.get("ip")!=null && !"".equals(searchMap.get("ip"))){
                criteria.andLike("ip","%"+searchMap.get("ip")+"%");
            }
            // nickname
            if(searchMap.get("nickname")!=null && !"".equals(searchMap.get("nickname"))){
                criteria.andLike("nickname","%"+searchMap.get("nickname")+"%");
            }
            // url
            if(searchMap.get("url")!=null && !"".equals(searchMap.get("url"))){
                criteria.andLike("url","%"+searchMap.get("url")+"%");
            }
            // module
            if(searchMap.get("module")!=null && !"".equals(searchMap.get("module"))){
                criteria.andLike("module","%"+searchMap.get("module")+"%");
            }
            // method
            if(searchMap.get("method")!=null && !"".equals(searchMap.get("method"))){
                criteria.andLike("method","%"+searchMap.get("method")+"%");
            }
            // action
            if(searchMap.get("action")!=null && !"".equals(searchMap.get("action"))){
                criteria.andLike("action","%"+searchMap.get("action")+"%");
            }
            // description
            if(searchMap.get("description")!=null && !"".equals(searchMap.get("description"))){
                criteria.andLike("description","%"+searchMap.get("description")+"%");
            }
            // create
            if(searchMap.get("create")!=null && !"".equals(searchMap.get("create"))){
                criteria.andLike("create","%"+searchMap.get("create")+"%");
            }
            // status
            if(searchMap.get("status")!=null && !"".equals(searchMap.get("status"))){
                criteria.andLike("status","%"+searchMap.get("status")+"%");
            }

            // id
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }

        }
        return example;
    }

}
