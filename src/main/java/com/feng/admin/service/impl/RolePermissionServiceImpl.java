package com.feng.admin.service.impl;

import com.feng.admin.entity.RolePermission;
import com.feng.admin.mapper.RolePermissionMapper;
import com.feng.admin.service.RolePermissionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class RolePermissionServiceImpl implements RolePermissionService {

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<RolePermission> findAll() {
        return rolePermissionMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageInfo<RolePermission> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<RolePermission> pageInfo=new PageInfo<>(rolePermissionMapper.selectAll());
        return pageInfo;
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<RolePermission> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return rolePermissionMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageInfo<RolePermission> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        PageInfo<RolePermission> pageInfo=new PageInfo<>(rolePermissionMapper.selectByExample(example));
        return pageInfo;
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public RolePermission findById(Long id) {
        return rolePermissionMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param rolePermission
     */
    public void add(RolePermission rolePermission) {
        rolePermissionMapper.insert(rolePermission);
    }

    /**
     * 修改
     * @param rolePermission
     */
    public void update(RolePermission rolePermission) {
        rolePermissionMapper.updateByPrimaryKeySelective(rolePermission);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Long id) {
        rolePermissionMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(RolePermission.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){


        }
        return example;
    }

}
