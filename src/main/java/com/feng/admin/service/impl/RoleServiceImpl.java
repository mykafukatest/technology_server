package com.feng.admin.service.impl;

import cn.hutool.core.date.DateUtil;
import com.beust.jcommander.internal.Maps;
import com.feng.admin.entity.Permission;
import com.feng.admin.entity.Role;
import com.feng.admin.entity.RolePermission;
import com.feng.admin.mapper.PermissionMapper;
import com.feng.admin.mapper.RoleMapper;
import com.feng.admin.mapper.RolePermissionMapper;
import com.feng.admin.service.RoleService;
import com.feng.admin.utils.TreeUtils;
import com.feng.admin.vo.PermissionVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author  lbl
 * @date  2020/5/13 13:22
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Override
    public int updateBatch(List<Role> list) {
        return roleMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Role> list) {
        return roleMapper.batchInsert(list);
    }

    @Override
    public int insertOrUpdate(Role record) {
        return roleMapper.insertOrUpdate(record);
    }

    @Override
    public int insertOrUpdateSelective(Role record) {
        return roleMapper.insertOrUpdateSelective(record);
    }

    @Override
    public  Map<String,Object> queryMenuBasedOnRoleId(Long roleId) {
        List<Permission> permissionsAll = permissionMapper.selectAll();
        List<Long> selectedId = permissionMapper.queryPermissionsBasedOnRoleId(roleId);
        Map<String, Object> result = Maps.newHashMap();
        List<PermissionVo> vos = TreeUtils
                .permissionToVo(permissionsAll);
        result.put("allMenus",TreeUtils.treeMenuConversionBy(vos));
        result.put("selectIds",selectedId);
        return result;
    }

    @Override
    @Transactional
    public void addAuthorityByRoleId(Long roleId, List<Long> menuList) {
        Example example=new Example(RolePermission.class);
        example.createCriteria().andEqualTo("roleId",roleId);
        rolePermissionMapper.deleteByExample(example);
        for (Long id : menuList) {
            RolePermission rolePermission=new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermissionId(id);

            rolePermissionMapper.insert(rolePermission);
        }
    }

    /**
     * 返回全部记录
     * @return
     */
    public List<Role> findAll() {
        return roleMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageInfo<Role> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<Role> pageInfo=new PageInfo<>(roleMapper.selectAll());
        return pageInfo;
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Role> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return roleMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageInfo<Role> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        PageInfo<Role> pageInfo=new PageInfo<>(roleMapper.selectByExample(example));
        return pageInfo;
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Role findById(Long id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param role
     */
    public void add(Role role) {
        roleMapper.insert(role);
    }

    /**
     * 修改
     * @param role
     */
    public void update(Role role) {
        roleMapper.updateByPrimaryKeySelective(role);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Long id) {
        roleMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 角色名称
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 角色英文名称
            if(searchMap.get("enname")!=null && !"".equals(searchMap.get("enname"))){
                criteria.andLike("enname","%"+searchMap.get("enname")+"%");
            }
            // 备注
            if(searchMap.get("description")!=null && !"".equals(searchMap.get("description"))){
                criteria.andLike("description","%"+searchMap.get("description")+"%");
            }
            // 更新时间
            if(searchMap.get("updated")!=null ){
                criteria.andGreaterThanOrEqualTo("updated", DateUtil.beginOfDay(DateUtil.parse(String.valueOf(searchMap.get("updated")))));
            }

        }
        return example;
    }

}

