package com.feng.admin.service.impl;

import com.feng.admin.service.UserInfoConvert;
import com.feng.admin.vo.UserDTO;
import com.feng.admin.vo.UserDetailsVo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author lbl
 * @date 2020/6/7 10:58
 */
@Service
public class UserInfoConvertImpl implements UserInfoConvert {

    @Override
    public UserDTO jwtUserToUserDTO() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsVo detailsVo=(UserDetailsVo)authentication.getPrincipal();
        return new UserDTO(detailsVo.getUsername(),detailsVo.getName());
    }
}
