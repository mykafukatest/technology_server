package com.feng.admin.utils;

import cn.hutool.core.bean.BeanUtil;
import com.feng.admin.entity.Permission;
import com.feng.admin.vo.PermissionVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @date 2020/5/15 13:18
 */
public class TreeUtils {


    public static  List<PermissionVo> treeMenuConversion(List<Permission> t){
        List<PermissionVo> permissions = permissionToVo(t).stream().peek(e->e.setCname(String.valueOf(System.currentTimeMillis()))).collect(Collectors.toList());

        return treeMenuConversionBy(permissions);

    }

    public static  List<PermissionVo> treeMenuConversionBy(List<PermissionVo> permissions){

        List<PermissionVo> root = permissions.stream().
                filter(e -> Objects.isNull(e.getParentId()) || Objects.equals(0L, e.getParentId()))
                .collect(Collectors.toList());
        return findChildNodeRecursive(root,permissions);

    }

    public static List<PermissionVo> permissionToVo(List<Permission> t){
        return t.stream().map(e->{
            PermissionVo vo=new PermissionVo();
            BeanUtil.copyProperties(e,vo);
            return vo;
        }).collect(Collectors.toList());
    }

    private static List<PermissionVo>  findChildNodeRecursive(List<PermissionVo> root, List<PermissionVo> t) {
        List<PermissionVo> vos=new ArrayList<>();
        root.forEach(e->{
            List<PermissionVo> childNode=findChildNode(e,t);
            findChildNodeRecursive(childNode,t);
            e.setChildren(childNode);
            vos.add(e);
        });
        return vos;
    }

    private static List<PermissionVo> findChildNode(PermissionVo rootVo, List<PermissionVo> t) {
        return t.stream().filter(e->Objects.equals(rootVo.getId(),e.getParentId())).collect(Collectors.toList());
    }
}
