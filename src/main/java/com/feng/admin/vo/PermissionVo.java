package com.feng.admin.vo;

import com.feng.admin.entity.Permission;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PermissionVo extends Permission implements Serializable{

    /**
     * 子菜单
     */
    private List<PermissionVo> children;
    /**
     * 是否选择
     */
    private boolean isSelect;
    /**
     * 组件名称
     */
    private String cname;

}