package com.feng.admin.vo;

/**
 * @author : 汤北寒
 * @date : 2020-05-2020/5/20-20:42
 */
public class UserDTO {

    private String username;
    private String nickname;

    public UserDTO() {
    }

    public UserDTO(String username, String nickname) {
        this.username = username;
        this.nickname = nickname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
