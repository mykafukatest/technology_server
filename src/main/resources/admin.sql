/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : admin

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 08/01/2021 14:52:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_log`;
CREATE TABLE `tb_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `params` json COMMENT '执行入参',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '账号',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'IP地址',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '昵称',
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '访问url',
  `module` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '模块名称',
  `method` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '方法名称',
  `action` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '耗费时间',
  `description` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `create` datetime(0) DEFAULT NULL COMMENT '执行时间',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_log
-- ----------------------------
INSERT INTO `tb_log` VALUES (1, '{\"permission\": {\"id\": null, \"ico\": \"oauth2\", \"url\": \"/set/clientDetails\", \"name\": \"oauth2客户端配置\", \"type\": 1, \"enname\": \"CLIENTDETAILS_MANAGEMENT\", \"created\": null, \"updated\": null, \"parentId\": 1, \"description\": \"oauth2客户端配置\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '18ms', '执行成功', '2020-12-17 14:23:23', '1');
INSERT INTO `tb_log` VALUES (2, '{\"permission\": {\"id\": null, \"ico\": null, \"url\": \"#\", \"name\": \"1\", \"type\": 0, \"enname\": \"2\", \"created\": null, \"updated\": null, \"parentId\": 0, \"description\": \"1\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '7ms', '执行成功', '2020-12-17 14:25:12', '1');
INSERT INTO `tb_log` VALUES (3, '{\"permission\": {\"id\": null, \"ico\": null, \"url\": \"#\", \"name\": \"1\", \"type\": 0, \"enname\": \"2\", \"created\": null, \"updated\": null, \"parentId\": 0, \"description\": \"3\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '8ms', '执行成功', '2020-12-17 14:27:15', '1');
INSERT INTO `tb_log` VALUES (4, '{\"permission\": {\"id\": null, \"ico\": \"search\", \"url\": \"/clientDetails/search/{page}/{size}\", \"name\": \"oauth2查询\", \"type\": 2, \"enname\": \"clientDetails:search:page\", \"created\": null, \"updated\": null, \"parentId\": 71, \"description\": \"分页列表查询\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '5ms', '执行成功', '2020-12-17 14:35:36', '1');
INSERT INTO `tb_log` VALUES (5, '{\"permission\": {\"id\": null, \"ico\": \"edit\", \"url\": \"/clientDetails/update\", \"name\": \"oauth2修改\", \"type\": 2, \"enname\": \"clientDetails:update\", \"created\": null, \"updated\": null, \"parentId\": 71, \"description\": \"修改\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '17ms', '执行成功', '2020-12-17 14:43:21', '1');
INSERT INTO `tb_log` VALUES (6, '{\"permission\": {\"id\": null, \"ico\": \"delete\", \"url\": \"/clientDetails/delete\", \"name\": \"oauth2删除\", \"type\": 2, \"enname\": \"clientDetails:delete\", \"created\": null, \"updated\": null, \"parentId\": 71, \"description\": \"删除\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '7ms', '执行成功', '2020-12-17 14:43:59', '1');
INSERT INTO `tb_log` VALUES (7, '{\"permission\": {\"id\": null, \"ico\": \"add\", \"url\": \"/clientDetails/save\", \"name\": \"oauth2添加\", \"type\": 2, \"enname\": \"clientDetails:save\", \"created\": null, \"updated\": null, \"parentId\": 71, \"description\": \"oauth2添加\"}}', 'admin', '10.60.85.74', NULL, '/user_service/permission/save', '权限模块', 'add', '8ms', '执行成功', '2020-12-17 14:45:23', '1');

-- ----------------------------
-- Table structure for tb_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_permission`;
CREATE TABLE `tb_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父权限',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `enname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限英文名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '授权路径',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `created` datetime(0) NOT NULL,
  `updated` datetime(0) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '类型 0根1菜单2功能',
  `ico` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_permission
-- ----------------------------
INSERT INTO `tb_permission` VALUES (1, 0, '系统设置', 'SYSTEM_SETTINGS', '#', '系统设置', '2020-05-13 22:33:33', '2020-06-05 22:33:36', 0, 'setting');
INSERT INTO `tb_permission` VALUES (49, 1, '菜单管理', 'MENU_MANAGEMENT', '/set/menus', '菜单管理', '2020-05-22 13:51:39', '2020-05-22 13:51:39', 1, 'operation');
INSERT INTO `tb_permission` VALUES (50, 49, '菜单添加', 'MENU_ADD', '/permission/save', '菜单添加按钮功能', '2020-05-22 15:45:05', '2020-05-22 15:45:05', 2, 'add');
INSERT INTO `tb_permission` VALUES (51, 49, '菜单查询', 'MENU_QUERY', '/permission/search/{page}/{size}', '菜单查询功能', '2020-05-22 15:51:39', '2020-05-22 15:51:39', 2, 'search');
INSERT INTO `tb_permission` VALUES (52, 49, '菜单修改', 'MENU_EDIT', '/permission/update', '菜单修改功能', '2020-05-22 15:53:21', '2020-05-22 15:53:21', 2, 'edit');
INSERT INTO `tb_permission` VALUES (53, 49, '菜单删除', 'MENU_DELETE', '/permission/delete/{id}', '菜单删除功能', '2020-05-22 15:54:35', '2020-05-22 15:54:35', 2, 'delete');
INSERT INTO `tb_permission` VALUES (54, 1, '角色管理', 'ROLE_MANAGEMENT', '/set/role', '角色管理', '2020-05-25 13:40:44', '2020-05-25 13:40:44', 1, 'role');
INSERT INTO `tb_permission` VALUES (55, 1, '用户管理', 'USER_MANAGEMENT', '/set/user', '用户管理', '2020-05-26 13:09:24', '2020-05-26 13:09:24', 1, 'user');
INSERT INTO `tb_permission` VALUES (58, 1, '日志管理', 'LOG_MANAGEMENT', '/set/log', '日志管理', '2020-06-07 10:33:54', '2020-06-07 10:33:54', 1, 'setting');
INSERT INTO `tb_permission` VALUES (59, 54, '角色添加', 'ROLE_SAVE', '/role/save', '角色添加', '2020-06-08 20:38:11', '2020-06-08 20:38:11', 2, 'add');
INSERT INTO `tb_permission` VALUES (60, 54, '角色删除', 'ROLE_DELETE', '/role/delete/{id}', '角色删除', '2020-06-08 20:38:50', '2020-06-08 20:38:50', 2, 'delete');
INSERT INTO `tb_permission` VALUES (61, 54, '分配权限', 'ROLE_ADDAUTHORITYBYROLEID', '/role/addAuthorityByRoleId', '分配权限', '2020-06-08 20:40:26', '2020-06-08 20:40:26', 2, 'role');
INSERT INTO `tb_permission` VALUES (62, 54, '修改角色', 'ROLE_UPDATE', '/role/update', '修改角色', '2020-06-08 20:41:02', '2020-06-08 20:41:02', 2, 'edit');
INSERT INTO `tb_permission` VALUES (63, 54, '角色查询', 'ROLE_QUERY', '/role/search/{page}/{size}', '角色查询', '2020-06-08 20:43:27', '2020-06-08 20:43:27', 2, 'search');
INSERT INTO `tb_permission` VALUES (64, 55, '用户添加', 'USER_ADD', '/user/save', '用户添加', '2020-06-08 20:58:05', '2020-06-08 20:58:05', 2, 'add');
INSERT INTO `tb_permission` VALUES (65, 55, '用户删除', 'USER_DELETE', '/user/delete/{id}', '用户删除', '2020-06-08 20:59:54', '2020-06-08 20:59:54', 2, 'delete');
INSERT INTO `tb_permission` VALUES (66, 55, '用户修改', 'USER_UPDATE', '/role/update', '用户修改', '2020-06-08 21:00:37', '2020-06-08 21:00:37', 2, 'edit');
INSERT INTO `tb_permission` VALUES (67, 55, '用户查询', 'USER_QUERY', '/user/search/{page}/{size}', '用户查询', '2020-06-08 21:02:04', '2020-06-08 21:02:04', 2, 'search');
INSERT INTO `tb_permission` VALUES (68, 58, '日志新增', 'LOG_ADD', '/log/save', '日志模块', '2020-06-08 21:08:54', '2020-06-08 21:08:54', 2, 'add');
INSERT INTO `tb_permission` VALUES (69, 58, '日志查询', 'LOG_QUERY', '/log/search/{page}/{size}', '日志查询', '2020-06-08 21:14:29', '2020-06-08 21:14:29', 2, 'search');
INSERT INTO `tb_permission` VALUES (70, 58, '日志删除', 'LOG_DELETE', '/log/delete/{id}', '日志删除', '2020-06-08 21:15:19', '2020-06-08 21:15:19', 2, 'delete');

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父角色',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `enname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色英文名称',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `created` datetime(0) NOT NULL,
  `updated` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (37, 0, '超级管理员', 'admin', '超级管理员最高权限', '2019-04-04 23:22:03', '2020-06-08 21:34:42');
INSERT INTO `tb_role` VALUES (38, 0, '系统管理员', 'system_admin', '系统管理员', '2020-05-24 08:46:11', '2020-05-24 08:46:11');

-- ----------------------------
-- Table structure for tb_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_permission`;
CREATE TABLE `tb_role_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL COMMENT '角色 ID',
  `permission_id` bigint(20) NOT NULL COMMENT '权限 ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 280 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role_permission
-- ----------------------------
INSERT INTO `tb_role_permission` VALUES (196, 38, 1);
INSERT INTO `tb_role_permission` VALUES (197, 38, 49);
INSERT INTO `tb_role_permission` VALUES (198, 38, 51);
INSERT INTO `tb_role_permission` VALUES (199, 38, 54);
INSERT INTO `tb_role_permission` VALUES (200, 38, 59);
INSERT INTO `tb_role_permission` VALUES (201, 38, 60);
INSERT INTO `tb_role_permission` VALUES (202, 38, 61);
INSERT INTO `tb_role_permission` VALUES (203, 38, 62);
INSERT INTO `tb_role_permission` VALUES (204, 38, 63);
INSERT INTO `tb_role_permission` VALUES (205, 38, 55);
INSERT INTO `tb_role_permission` VALUES (206, 38, 67);
INSERT INTO `tb_role_permission` VALUES (207, 38, 58);
INSERT INTO `tb_role_permission` VALUES (208, 38, 69);
INSERT INTO `tb_role_permission` VALUES (254, 37, 1);
INSERT INTO `tb_role_permission` VALUES (255, 37, 49);
INSERT INTO `tb_role_permission` VALUES (256, 37, 50);
INSERT INTO `tb_role_permission` VALUES (257, 37, 51);
INSERT INTO `tb_role_permission` VALUES (258, 37, 52);
INSERT INTO `tb_role_permission` VALUES (259, 37, 53);
INSERT INTO `tb_role_permission` VALUES (260, 37, 54);
INSERT INTO `tb_role_permission` VALUES (261, 37, 59);
INSERT INTO `tb_role_permission` VALUES (262, 37, 60);
INSERT INTO `tb_role_permission` VALUES (263, 37, 61);
INSERT INTO `tb_role_permission` VALUES (264, 37, 62);
INSERT INTO `tb_role_permission` VALUES (265, 37, 63);
INSERT INTO `tb_role_permission` VALUES (266, 37, 55);
INSERT INTO `tb_role_permission` VALUES (267, 37, 64);
INSERT INTO `tb_role_permission` VALUES (268, 37, 65);
INSERT INTO `tb_role_permission` VALUES (269, 37, 66);
INSERT INTO `tb_role_permission` VALUES (270, 37, 67);
INSERT INTO `tb_role_permission` VALUES (271, 37, 58);
INSERT INTO `tb_role_permission` VALUES (272, 37, 68);
INSERT INTO `tb_role_permission` VALUES (273, 37, 69);
INSERT INTO `tb_role_permission` VALUES (274, 37, 70);
INSERT INTO `tb_role_permission` VALUES (275, 37, 71);
INSERT INTO `tb_role_permission` VALUES (276, 37, 74);
INSERT INTO `tb_role_permission` VALUES (277, 37, 75);
INSERT INTO `tb_role_permission` VALUES (278, 37, 76);
INSERT INTO `tb_role_permission` VALUES (279, 37, 77);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码，加密存储',
  `avatar` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '注册手机号',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '123' COMMENT '注册邮箱',
  `created` datetime(0) NOT NULL,
  `updated` datetime(0) NOT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '简介',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名字',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (37, 'admin', '$2a$10$Gh7/ndQecUhMKJpOZyRqLOT4947o4oJKXc2Vf3dZ0FVH0xgh7uwSm', 'http://47.106.98.232:8888/group1/M00/00/00/rBD8OF-krX6AZ7m_AAfhSFIV0k4640.jpg', '15888888888', 'lee.lusifer@gmail.com', '2019-04-04 23:21:27', '2019-04-04 23:21:29', '1', '管理员');
INSERT INTO `tb_user` VALUES (38, 'admin1', '$2a$10$ZcWXCBKtNfKHyXTQ0ZuqWeBnbUj3P2ywihoBeMBHG2iruGzGLFo/m', 'http://47.106.98.232:8888/group1/M00/00/00/rBD8OF-krfmAT9shAAFeF90OfCc351.jpg', '158888888881', '1', '2019-04-04 23:21:27', '2019-04-04 23:21:29', NULL, '低级');
INSERT INTO `tb_user` VALUES (39, 'admin12', '$2a$10$ZcWXCBKtNfKHyXTQ0ZuqWeBnbUj3P2ywihoBeMBHG2iruGzGLFo/m', 'http://47.106.98.232:8888/group1/M00/00/00/rBD8OF-k3xyAFBZ7AAQ1X-fcUFc875.png', '1588888888821', '', '2019-04-04 23:21:27', '2019-04-04 23:21:29', NULL, '测试管理员啊啊啊啊');
INSERT INTO `tb_user` VALUES (40, '2456709177@qq.com', '$2a$10$ViZsFpjD4pfqosJzljSq..h7nhqSDNEmjHb8atjbZ9jXPjHx49VpG', 'http://47.106.98.232:8888/group1/M00/00/00/rBD8OF-k3y-AHHF3AAAbM5hUq4U572.png', '15279524230', '2456709177@qq.com', '2020-11-03 10:30:22', '2020-11-03 10:30:22', NULL, '张三');

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户 ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色 ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (49, 37, 38);
INSERT INTO `tb_user_role` VALUES (50, 37, 37);

SET FOREIGN_KEY_CHECKS = 1;
