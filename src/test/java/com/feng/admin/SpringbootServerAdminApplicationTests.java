package com.feng.admin;

import com.feng.admin.utils.FastDFSClientUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

@SpringBootTest
class SpringbootServerAdminApplicationTests {

    @Autowired
    private FastDFSClientUtil fastDFSClientUtil;


    @Test
    void contextLoads() throws IOException {
        String s = fastDFSClientUtil.uploadFile(new File("C:\\Users\\liuzuming\\Pictures\\123.jpg"));
        System.out.println(s);
    }

}
